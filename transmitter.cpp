#include "transmitter.h"
#include <QtDebug>

Transmitter::Transmitter(QObject *parent) :
    QThread(parent),
    m_progress(0),
    m_maximumTime(0.0),
    m_minimumTime(0.0),
    m_averageTime(0.0),
    m_sentPackets(0),
    m_receivedPackets(0),
    m_lostPackets(0),
    m_destinationAddress("192.168.1.100"),
    m_destinationPort(1001),
    m_packetCount(100),
    m_packetSize(100),
    m_interval(10),
    m_chronoInterval(static_cast<double>(m_interval) / 1000.0),
    m_packetDescriptorArray(m_packetSize),
    m_hostAddress(m_destinationAddress)
{
    m_data = new char[m_packetSize];
    setTerminationEnabled();
}

Transmitter::~Transmitter()
{
    delete[] m_data;
    delete m_socket;
}

void Transmitter::run()
{
    resetSocket();

    uint16_t _counter = 0;

    for(QVector<PacketDescriptor>::iterator it = m_packetDescriptorArray.begin(); it != m_packetDescriptorArray.end(); ++it)
    {
        sendOnePacket(_counter++, &(*it));
    }

    calculateStatistics();
    emitStatistics();

}

void Transmitter::setDestinationAddress(const QString &destinationAddress)
{
    m_destinationAddress = destinationAddress;
    m_hostAddress = QHostAddress(m_destinationAddress);
}

void Transmitter::setDestinationPort(const int &destinationPort)
{
    m_destinationPort = destinationPort;
}

void Transmitter::setPacketCount(const int &packetCount)
{
    m_packetCount = packetCount;
    m_packetDescriptorArray.resize(m_packetCount);
    m_packetDescriptorArray.squeeze();
}

void Transmitter::setPacketSize(const int &packetSize)
{
    m_packetSize = packetSize;
    delete[] m_data;
    m_data = new char[m_packetSize];
}

void Transmitter::setInterval(const int &interval)
{
    m_interval = interval;
    m_chronoInterval = std::chrono::duration<double>(static_cast<double>(m_interval) / 1000.0);
}

void Transmitter::resetPacketDescriptors()
{
    m_packetDescriptorArray.fill(PacketDescriptor());
}

void Transmitter::calculateStatistics()
{
    m_receivedPackets = m_sentPackets = m_lostPackets = 0;
    m_maximumTime = m_minimumTime = m_averageTime = 0.0;
    for(QVector<PacketDescriptor>::iterator it = m_packetDescriptorArray.begin(); it != m_packetDescriptorArray.end(); ++it)
    {
        m_sentPackets++;

        if((*it).m_arrived)
        {
            m_receivedPackets++;
            if((*it).m_deltaTime > m_maximumTime)
            {
                m_maximumTime = (*it).m_deltaTime;
            }
            else if((*it).m_deltaTime < m_minimumTime)
            {
                m_minimumTime = (*it).m_deltaTime;
            }
            m_averageTime += (*it).m_deltaTime;
        }
    }
    if(m_receivedPackets != 0)
        m_averageTime /= m_receivedPackets;
    else
        m_averageTime = 0.0;
    m_lostPackets = m_sentPackets - m_receivedPackets;
}

void Transmitter::emitStatistics()
{
    emit maximumTimeChanged(m_maximumTime);
    emit minimumTimeChanged(m_minimumTime);
    emit averageTimeChanged(m_averageTime);
    emit sentPacketsChanged(m_sentPackets);
    emit receivedPacketsChanged(m_receivedPackets);
    emit lostPacketsChanged(m_lostPackets);
}

void Transmitter::sendOnePacket(const int &counter, PacketDescriptor *packetDescriptor)
{
    using namespace std::chrono;

    high_resolution_clock::time_point _startTime = high_resolution_clock::now();
    high_resolution_clock::time_point _endTime = high_resolution_clock::now();
    duration<double> _duration;
    bool _timeoutHit = false;

    packetDescriptor->m_number = counter;
    m_data[0] = reinterpret_cast<const char *>(&counter)[0];
    m_data[1] = reinterpret_cast<const char *>(&counter)[1];

    _startTime = high_resolution_clock::now();

    m_socket->writeDatagram(m_data, m_packetSize, m_hostAddress, m_destinationPort);

    _timeoutHit = high_resolution_clock::now() - _startTime > m_chronoInterval;
    while(!(m_socket->hasPendingDatagrams()) && !_timeoutHit)
    {
        _timeoutHit = high_resolution_clock::now() - _startTime > m_chronoInterval;
    }

    emit progressChanged(counter + 1);

    if(_timeoutHit)
        return;

    _endTime = high_resolution_clock::now();
    _duration = (_endTime - _startTime);
    packetDescriptor->m_deltaTime = _duration.count();
    packetDescriptor->m_arrived = true;


    while(high_resolution_clock::now() - _startTime < m_chronoInterval);
}

void Transmitter::resetSocket()
{
    delete m_socket;
    m_socket = new QUdpSocket();
    m_socket->abort();
    m_socket->bind(QHostAddress::AnyIPv4, trLISTEN_PORT);
}
