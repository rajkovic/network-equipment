import socket as soc

sock = soc.socket(soc.AF_INET, soc.SOCK_DGRAM)

server_address = '127.0.0.1'

server_port = 1001

server_param = (server_address, server_port)

sock.bind(server_param)

while True:
	receivedPayload, clientAddress = sock.recvfrom(10000)
	sentBytes = sock.sendto(receivedPayload, clientAddress)
	print("Sent " + str(sentBytes) + " bytes to " + str(clientAddress[0]) + ":" + str(clientAddress[1]))