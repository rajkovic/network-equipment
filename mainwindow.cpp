#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setMaximumLatency(const double &latency)
{
    ui->maximumLatencyDoubleSpinBox->setValue(latency);
}

void MainWindow::setMinimumLatency(const double &latency)
{
    ui->minimumLatencyDoubleSpinBox->setValue(latency);
}

void MainWindow::setAverageLatency(const double &latency)
{
    ui->averageLatencyDoubleSpinBox->setValue(latency);
}

void MainWindow::setSentPackets(const int &packets)
{
    ui->packetsSentSpinBox->setValue(packets);
}

void MainWindow::setReceivedPackets(const int &packets)
{
    ui->packetsReceivedSpinBox->setValue(packets);
}

void MainWindow::setLostPackets(const int &packets)
{
    ui->packetsLostSpinBox->setValue(packets);
}

void MainWindow::setProgress(const int &progress)
{
    ui->progressBar->setValue(progress);
    if(progress == (ui->progressBar->maximum()))
    {
        ui->startPushButton->setDisabled(false);
        ui->stopPushButton->setDisabled(true);
        ui->destinationAddressLineEdit->setDisabled(false);
        ui->destinationAddressLabel->setDisabled(false);
        ui->destinationPortSpinBox->setDisabled(false);
        ui->destinationPortLabel->setDisabled(false);
        ui->packetCountSpinBox->setDisabled(false);
        ui->packetCountLabel->setDisabled(false);
        ui->packetSizeSpinBox->setDisabled(false);
        ui->packetSizeLabel->setDisabled(false);
        ui->intervalSpinBox->setDisabled(false);
        ui->intervalLabel->setDisabled(false);
        ui->progressBar->setDisabled(true);
    }
}

void MainWindow::on_startPushButton_clicked()
{
    emit startExecution();
    ui->startPushButton->setDisabled(true);
    ui->stopPushButton->setDisabled(false);
    ui->destinationAddressLineEdit->setDisabled(true);
    ui->destinationAddressLabel->setDisabled(true);
    ui->destinationPortSpinBox->setDisabled(true);
    ui->destinationPortLabel->setDisabled(true);
    ui->packetCountSpinBox->setDisabled(true);
    ui->packetCountLabel->setDisabled(true);
    ui->packetSizeSpinBox->setDisabled(true);
    ui->packetSizeLabel->setDisabled(true);
    ui->intervalSpinBox->setDisabled(true);
    ui->intervalLabel->setDisabled(true);
    ui->progressBar->setDisabled(false);
}

void MainWindow::on_stopPushButton_clicked()
{
    emit stopExecution();
    ui->startPushButton->setDisabled(false);
    ui->stopPushButton->setDisabled(true);
    ui->destinationAddressLineEdit->setDisabled(false);
    ui->destinationAddressLabel->setDisabled(false);
    ui->destinationPortSpinBox->setDisabled(false);
    ui->destinationPortLabel->setDisabled(false);
    ui->packetCountSpinBox->setDisabled(false);
    ui->packetCountLabel->setDisabled(false);
    ui->packetSizeSpinBox->setDisabled(false);
    ui->packetSizeLabel->setDisabled(false);
    ui->intervalSpinBox->setDisabled(false);
    ui->intervalLabel->setDisabled(false);
    ui->progressBar->setDisabled(true);
}

void MainWindow::on_destinationAddressLineEdit_textChanged(QString text)
{
    emit destinationAddressChanged(text);
}

void MainWindow::on_destinationPortSpinBox_valueChanged(int value)
{
    emit destinationPortChanged(value);
}

void MainWindow::on_packetCountSpinBox_valueChanged(int value)
{
    emit packetCountChanged(value);
    ui->progressBar->setMaximum(value);
}

void MainWindow::on_packetSizeSpinBox_valueChanged(int value)
{
    emit packetSizeChanged(value);
}

void MainWindow::on_intervalSpinBox_valueChanged(int value)
{
    emit intervalChanged(value);
}
