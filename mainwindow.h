#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThread>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
signals:
    void startExecution(QThread::Priority priority = QThread::InheritPriority);
    void stopExecution();
    void destinationAddressChanged(const QString &text);
    void destinationPortChanged(const int &value);
    void packetCountChanged(const int &value);
    void packetSizeChanged(const int &value);
    void intervalChanged(const int &value);
public slots:
    void setMaximumLatency(const double &latency);
    void setMinimumLatency(const double &latency);
    void setAverageLatency(const double &latency);

    void setSentPackets(const int &packets);
    void setReceivedPackets(const int &packets);
    void setLostPackets(const int &packets);

    void setProgress(const int &progress);
private slots:
    void on_startPushButton_clicked();
    void on_stopPushButton_clicked();
    void on_destinationAddressLineEdit_textChanged(QString text);
    void on_destinationPortSpinBox_valueChanged(int value);
    void on_packetCountSpinBox_valueChanged(int value);
    void on_packetSizeSpinBox_valueChanged(int value);
    void on_intervalSpinBox_valueChanged(int value);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
