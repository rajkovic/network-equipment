#include "mainwindow.h"
#include <QApplication>

#include "transmitter.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    Transmitter packetTransmitter;

    // Make connections
    QObject::connect(&packetTransmitter, &Transmitter::progressChanged, &w, &MainWindow::setProgress);
    QObject::connect(&packetTransmitter, &Transmitter::maximumTimeChanged, &w, &MainWindow::setMaximumLatency);
    QObject::connect(&packetTransmitter, &Transmitter::minimumTimeChanged, &w, &MainWindow::setMinimumLatency);
    QObject::connect(&packetTransmitter, &Transmitter::averageTimeChanged, &w, &MainWindow::setAverageLatency);
    QObject::connect(&packetTransmitter, &Transmitter::sentPacketsChanged, &w, &MainWindow::setSentPackets);
    QObject::connect(&packetTransmitter, &Transmitter::receivedPacketsChanged, &w, &MainWindow::setReceivedPackets);
    QObject::connect(&packetTransmitter, &Transmitter::lostPacketsChanged, &w, &MainWindow::setLostPackets);

    QObject::connect(&w, &MainWindow::startExecution, &packetTransmitter, &Transmitter::start);
    QObject::connect(&w, &MainWindow::stopExecution, &packetTransmitter, &Transmitter::terminate);
    QObject::connect(&w, &MainWindow::destinationAddressChanged, &packetTransmitter, &Transmitter::setDestinationAddress);
    QObject::connect(&w, &MainWindow::destinationPortChanged, &packetTransmitter, &Transmitter::setDestinationPort);
    QObject::connect(&w, &MainWindow::packetCountChanged, &packetTransmitter, &Transmitter::setPacketCount);
    QObject::connect(&w, &MainWindow::packetSizeChanged, &packetTransmitter, &Transmitter::setPacketSize);
    QObject::connect(&w, &MainWindow::intervalChanged, &packetTransmitter, &Transmitter::setInterval);
    return a.exec();
}
