#ifndef TRANSMITTER_H
#define TRANSMITTER_H

#include <QThread>
#include <QUdpSocket>
#include <QString>
#include <QVector>
#include <cinttypes>
#include <chrono>

#define trLISTEN_PORT 1000

struct PacketDescriptor
{
    PacketDescriptor() :
        m_number(0),
        m_deltaTime(0.0),
        m_arrived(false)
    {

    }
    uint16_t m_number;
    double m_deltaTime;
    bool m_arrived;
};

class Transmitter : public QThread
{
    Q_OBJECT
public:
    explicit Transmitter(QObject *parent = nullptr);
    ~Transmitter();

    void run() override;

signals:
    void progressChanged(const int &progress);

    void maximumTimeChanged(const double &maximumTime);
    void minimumTimeChanged(const double &minimumTime);
    void averageTimeChanged(const double &averageTime);

    void sentPacketsChanged(const int &sentPackets);
    void receivedPacketsChanged(const int &receivedPackets);
    void lostPacketsChanged(const int &lostPackets);

public slots:
    void setDestinationAddress(const QString &destinationAddress);
    void setDestinationPort(const int &destinationPort);

    void setPacketCount(const int &packetCount);
    void setPacketSize(const int &packetSize);

    void setInterval(const int &inteval);

private:
    void resetSocket();
    void resetPacketDescriptors();

    void calculateStatistics();
    void emitStatistics();

    void sendPackets();
    void sendOnePacket(const int &counter, PacketDescriptor *packetDescriptor);

private:
    int m_progress;
    double m_maximumTime;
    double m_minimumTime;
    double m_averageTime;
    int m_sentPackets;
    int m_receivedPackets;
    int m_lostPackets;

    QString m_destinationAddress;
    int m_destinationPort;

    int m_packetCount;
    int m_packetSize;

    int m_interval;
    std::chrono::duration<double> m_chronoInterval;

    QVector<PacketDescriptor> m_packetDescriptorArray;

    char *m_data;
    QUdpSocket *m_socket;
    QHostAddress m_hostAddress;
};

#endif // TRANSMITTER_H
